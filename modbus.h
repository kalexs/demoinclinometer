#include "stm32f0xx.h"
#include "usart.h"

#define _R_COIL_STATUS 0x01
#define _R_DISCRETE_INPUT 0x02
#define _R_HOLDING_REGISTERS 0x03
#define _R_INPUT_REGISTERS 0x04

#define _ADDR 0x01

#define _ANG_X_REG 30001;
#define _ANG_Y_REG 30004;

volatile static uint8_t device_addr;
volatile static uint8_t command_code;
volatile static uint8_t reg_addr0;
volatile static uint8_t reg_addr1;
volatile static uint8_t number0;
volatile static uint8_t number1;

void ParseMessage(void);

void ReadCoilStats (void);

void ReadDiscreteInput (void);

void ReadHoldingRegisters (void);

void ReadInputRegisters (void);

void TypeOfCommand(void);

int Check_CRC (void);
