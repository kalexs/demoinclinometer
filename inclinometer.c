#include "inclinometer.h"

void InclinometerInit ()
{
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
	
		//SPI1 CLK, IN, OUT	
	GPIOB->MODER &= ~ (GPIO_MODER_MODER3 | GPIO_MODER_MODER4 | GPIO_MODER_MODER5);
	GPIOB->OTYPER &= ~ (GPIO_OTYPER_OT_3 | GPIO_OTYPER_OT_4 | GPIO_OTYPER_OT_5);
	GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR3 | GPIO_OSPEEDR_OSPEEDR4 | GPIO_OSPEEDR_OSPEEDR5;
	GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR3 | GPIO_PUPDR_PUPDR4 | GPIO_PUPDR_PUPDR5);
	GPIOB->AFR[0] &= ~ (GPIO_AFRL_AFRL3 | GPIO_AFRL_AFRL4 | GPIO_AFRL_AFRL5);
	GPIOB->MODER |= GPIO_MODER_MODER3_1 | GPIO_MODER_MODER4_1 | GPIO_MODER_MODER5_1;
	
		//SPI1 CS
	GPIOA->MODER &= ~ (GPIO_MODER_MODER15);
	GPIOA->MODER |= GPIO_MODER_MODER15_0;
	GPIOA->OTYPER &= ~ (GPIO_OTYPER_OT_15);
	GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR15;
	GPIOA->PUPDR &= ~(GPIO_PUPDR_PUPDR15);
	
	RCC->APB2ENR |= RCC_APB2ENR_SPI1EN;
	SPI1->CR1 |= SPI_CR1_SSM; // slave select manager - software by SSI
	SPI1->CR1 |= SPI_CR1_SSI;
	SPI1->CR1 |= (SPI_CR1_BR_0 | SPI_CR1_BR_1 | SPI_CR1_BR_2);//serial clock band rate 256
	SPI1->CR1 |= SPI_CR1_CPOL; //high-level idle state
	SPI1->CR1 |= SPI_CR1_CPHA; //rising edge
	SPI1->CR1 |= SPI_CR1_MSTR; //master
	SPI1->CR2 &= ~(SPI_CR2_DS_0 | SPI_CR2_DS_1 | SPI_CR2_DS_2 | SPI_CR2_DS_3);
	SPI1->CR2 |= SPI_CR2_DS_0 | SPI_CR2_DS_1 | SPI_CR2_DS_2 | SPI_CR2_FRXTH;
	SPI1->CR1 |= SPI_CR1_SPE; //SPI2 enable
}

uint8_t sendByte_spi1(uint8_t send_data)
{
	uint8_t * _DR = (uint8_t*)(&SPI1->DR);
	uint8_t rec_data = 0;
	 
	while (!(SPI1->SR & SPI_SR_TXE));
	*_DR = send_data;
	while (!(SPI1->SR & SPI_SR_RXNE));
	rec_data = *_DR;
	
	return rec_data;
}

uint8_t acc_read1_spi1(uint8_t addr)
{
	uint8_t send_addr = 0;
	uint8_t rec_data = 0;
	
	send_addr |= 1<<7; //receive data
	send_addr |= addr;

	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(send_addr);
	rec_data = sendByte_spi1(TRASH);
	GPIOA->BSRR = GPIO_BSRR_BS_15;
	
	return rec_data;
}

int16_t acc_read2_spi1(uint8_t addr)
{
	uint8_t send_addr = 0;
	uint8_t rec_data[2];
	int16_t rec_data16;
	send_addr |= 3<<6; //receive data
	send_addr |= addr;

	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(send_addr);
	rec_data[0] = sendByte_spi1(TRASH); 
	rec_data[1] = sendByte_spi1(TRASH);
	GPIOA->BSRR = GPIO_BSRR_BS_15;
	
	rec_data16 = rec_data[0] | (rec_data[1] << 8);
	rec_data16 = (rec_data16>>6);

	return rec_data16;
}

void acc_write1_spi1(uint8_t addr, uint8_t send_data)
{
	uint8_t send_addr = 0;
	
	send_addr |= 0<<7; //send data
	send_addr |= addr;
	GPIOA->BSRR = GPIO_BSRR_BR_15;
	sendByte_spi1(send_addr);
	sendByte_spi1(send_data);
	GPIOA->BSRR = GPIO_BSRR_BS_15;
}

int16_t ang_x_detect (int16_t acc_data_x_16, int16_t acc_data_z_16)
{
		int16_t ang_x;
	
		if ((acc_data_x_16 > 130)&&(acc_data_x_16<270))
		{
			if (acc_data_z_16>=0)
			{
				ang_x = 90 - ang[acc_data_z_16];
			}
			if (acc_data_z_16<0)
			{
				ang_x = 90 + ang[-acc_data_z_16];
			}
		}
		
		if ((acc_data_x_16<-130)&&(acc_data_x_16>-270))
		{
			if (acc_data_z_16>=0)
			{
				ang_x = -90 + ang[acc_data_z_16];
			}
			if (acc_data_z_16<0)
			{
				ang_x = -90 - ang[-acc_data_z_16];
			}			
		}
		
		if ((acc_data_x_16>=0)&&(acc_data_x_16<=130))
		{
			if (acc_data_z_16>=0)
			{
				ang_x = ang[acc_data_x_16];
			}
			if (acc_data_z_16<0)
			{
				ang_x = 180 - ang[acc_data_x_16];
			}	
		}
		
		if ((acc_data_x_16<0)&&(acc_data_x_16>-130))
		{
			if (acc_data_z_16>=0)
			{
				ang_x = -ang[-acc_data_x_16];
			}
			if (acc_data_z_16<0)
			{
				ang_x = -180 + ang[-acc_data_x_16];
			}	
		}
		
		return ang_x;
}

int16_t ang_y_detect (int16_t acc_data_y_16, int16_t acc_data_z_16)
{
		int16_t ang_y = 0;
	
		if ((acc_data_y_16 > 130)&&(acc_data_y_16<270))
		{
	
			if (acc_data_z_16>=0)
			{
				ang_y = 90 - ang[acc_data_z_16];
			}
			if (acc_data_z_16<0)
			{
				ang_y = 90 + ang[-acc_data_z_16];
			}
		}
		
		if ((acc_data_y_16<-130)&&(acc_data_y_16>-270))
		{

			if (acc_data_z_16>=0)
			{
				ang_y = -90 + ang[acc_data_z_16];
			}
			if (acc_data_z_16<0)
			{
				ang_y = -90 - ang[-acc_data_z_16];
			}			
		}
		
		if ((acc_data_y_16>=0)&&(acc_data_y_16<=130))
		{
		
			if (acc_data_z_16>=0)
			{
				ang_y = ang[acc_data_y_16];
			}
			if (acc_data_z_16<0)
			{
				ang_y = 180 - ang[acc_data_y_16];
			}	
		}
		
		if ((acc_data_y_16<0)&&(acc_data_y_16>-130))
		{
			
			if (acc_data_z_16>=0)
			{
				ang_y = -ang[-acc_data_y_16];
			}
			if (acc_data_z_16<0)
			{
				ang_y = -180 + ang[-acc_data_y_16]; 
			}	
		}
		
		return ang_y;
}


int16_t acc_data_detect (uint8_t addr, int16_t offset)
{
	int16_t data = 0;
	
	int16_t sum = 0;
	
	for (int l=0; l<AVER; l++)
	{
		acc_write1_spi1(0x2D, 0x08);
		acc_write1_spi1(0x31, 0x04);
		sum+=acc_read2_spi1(addr)+offset;
	}
	
	data = sum / AVER;
	
	if (data > 262)
		data = 263;
	
	if (data < -262)
		data = -263;
	
	return data;
}

void GetAngles ()
{
	int16_t acc_data_x_16 = 0;
	int16_t acc_data_y_16 = 0;
	int16_t acc_data_z_16 = 0;
	
	int16_t ang_x = 0;
	int16_t ang_y = 0;
	

		acc_data_x_16 = acc_data_detect(0x32, offset_x); //-22
		acc_data_y_16 = acc_data_detect(0x34, offset_y); //-6
		acc_data_z_16 = acc_data_detect(0x36, offset_z); // 5
		
		
		ang_x = ang_x_detect(acc_data_x_16, acc_data_z_16);
		ang_y = ang_y_detect(acc_data_y_16, acc_data_z_16);	
		

		push_data(ang_x);
		push_data(ang_y);
		
//		uint8_t ang_x_LO = (uint8_t)ang_x;
//		uint8_t ang_x_HI = ang_x >> 8;
//		
//		uint8_t ang_y_LO = (uint8_t)ang_y;
//		uint8_t ang_y_HI = ang_y >> 8;
//		
//		push_char(ang_x_HI);
//		push_char(ang_x_LO);
//		push_char(ang_y_HI);
//		push_char(ang_y_LO);
//		
}

void calibrate()
{
	int16_t sum_x = 0;
	int16_t sum_y = 0;
	int16_t sum_z = 0;
	
	
	for (int o = 0; o<20; o++)
	{
		sum_x += acc_data_detect(0x32, offset_x);
		sum_y += acc_data_detect(0x34, offset_y);
		sum_z += acc_data_detect(0x36, offset_z);
	}
	
	offset_x = -sum_x / 20 - 1;
	offset_y = -sum_y / 20 - 1;
	offset_z = 260 - sum_z / 20;
}
