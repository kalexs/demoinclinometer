#include "usart.h"

void USARTInit()
{
	RCC->AHBENR |= RCC_AHBENR_GPIOCEN;
	RCC->AHBENR |= RCC_AHBENR_GPIODEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	
	GPIOA->MODER &= ~ (GPIO_MODER_MODER9 | GPIO_MODER_MODER10);
	GPIOA->OTYPER &= ~ (GPIO_OTYPER_OT_9 | GPIO_OTYPER_OT_10);
	GPIOA->PUPDR &= ~ (GPIO_PUPDR_PUPDR9 | GPIO_PUPDR_PUPDR10);
	GPIOA->AFR[1] &= ~ (GPIO_AFRH_AFRH1 | GPIO_AFRH_AFRH2);
	GPIOA->AFR[1] |= (1<<4);
	GPIOA->AFR[1] |= (1<<8);
	GPIOA->MODER |= GPIO_MODER_MODER9_1 | GPIO_MODER_MODER10_1;
	
	
	//USART RTS
	GPIOA->MODER &= ~ (GPIO_MODER_MODER12);
	GPIOA->OTYPER &= ~ (GPIO_OTYPER_OT_12);
	GPIOA->PUPDR &= ~ (GPIO_PUPDR_PUPDR12);
	GPIOA->AFR[0] &= ~ (GPIO_AFRH_AFRH4);
	GPIOA->AFR[0] |= (1<<16);
	GPIOA->MODER |= GPIO_MODER_MODER12_1;
	
	NVIC_EnableIRQ(USART1_IRQn);
	
	RCC->APB2ENR |= RCC_APB2ENR_USART1EN;
	USART1->CR1 |= USART_CR1_DEAT_0 | USART_CR1_DEAT_1 | USART_CR1_DEAT_2 | USART_CR1_DEAT_3 | USART_CR1_DEAT_4 | USART_CR1_DEDT_0 | USART_CR1_DEDT_1 | USART_CR1_DEDT_2 | USART_CR1_DEDT_3 | USART_CR1_DEDT_4 | USART_CR1_TE | USART_CR1_RE; 
	USART1->CR3 |= USART_CR3_DEM | USART_CR3_RTSE;
	USART1->BRR = SystemCoreClock/9600;
	USART1->CR1 |= USART_CR1_RXNEIE;
	USART1->RTOR |= ((uint32_t)0xFF000002U);
	USART1->CR2 |= USART_CR2_RTOEN;
	USART1->CR1 |= USART_CR1_UE;
	
	tr_iter = 0;
	re_iter = 0;
	
	re_flag = 0;
	tr_flag = 0;
	
	recieve_complete = 0;
}

void push_char (uint8_t data)
{
	while(tr_flag == 1) {};
	transmit_data[tr_iter] = data;
	tr_iter++;
	if (tr_iter>255) tr_iter=0;
}

void USART1_IRQHandler(void)
{
	if ((tr_flag == 1)&&((USART1->ISR & USART_ISR_TXE)))
	{
		if (tr_iter == transmit_buffer_size)
		{
			USART1->CR1 &= ~(USART_CR1_TXEIE);
			clear_transmit_data();
			tr_iter = 0;
			tr_flag = 0;
		}
		else
		{
			
			USART1->TDR = transmit_data[tr_iter];
			tr_iter++;
		}
	}
	
	if ((USART1->ISR & USART_ISR_RXNE))
	{
		USART1->CR1 |= USART_CR1_RTOIE;
		USART1->ICR |= USART_ICR_RTOCF;
		recieve_complete = 0;
		LEDOn(3);
		recieved_data[re_iter] = USART1->RDR;
		re_iter++;
		StartPeriod();
	}
	
	if (USART1->ISR & USART_ISR_RTOF)
	{
		USART1->CR1 &= ~(USART_CR1_RTOIE);
		recieve_complete = 1;
		LEDOff(3);
	}
}

uint8_t GetRecievedData(int n)
{
	if ((n>=0)&&(n<256))
	{
		return recieved_data[n];
	}
}

void clear_recieved_data ()
{
	
	for (int i = 0; i<re_iter; i++)
	{
		recieved_data[i] = 0x00;
	}
	
	re_iter = 0;
}

void clear_transmit_data ()
{
	
	for (int i = 0; i<tr_iter; i++)
	{
		transmit_data[i] = 0x00;
	}
	
	tr_iter = 0;
}

void SetTrIter (uint16_t n)
{
	tr_iter = n;
}

uint16_t GetReIter()
{
	return re_iter;
}

uint16_t GetReFlag()
{
	return re_flag;
}

void ThrowReFlag()
{
	re_flag = 0;
}

void SetTrFlag()
{
	tr_flag = 1;
}

uint16_t GetTrFlag()
{
	return tr_flag;
}

unsigned char * GetRecieveDataPointer ()
{
	return recieved_data;
}

uint16_t GetRecieveDataLen()
{
	return re_iter;
}

unsigned char * GetTransmitDataPointer ()
{
	return transmit_data;
}

uint16_t GetTransmitDataLen()
{
	return tr_iter;
}

void push_data (int16_t ang)
{
//		uint8_t buff[5];
//	
//		if (ang < 0)
//		{
//			ang = -ang;
//			push_char (0x2D);
//		}
//		
//		for (int j=0; j<5; j++)
//		{
//			buff[j] = ang % 10;
//			ang = ang / 10;
//		}
//		
//		for (int j=4; j>-1; j--)
//		{
//			push_char(0x30 + buff[j]);
//		}
		
		uint8_t ang_x_LO = (uint8_t)ang;
		uint8_t ang_x_HI = (uint8_t)(ang >> 8);
		
		push_char(ang_x_HI);
		push_char(ang_x_LO);
}

void TransmitStart()
{
	transmit_buffer_size = tr_iter;
	SetTrIter(0);
	SetTrFlag();
	USART1->CR1 |= USART_CR1_TXEIE;	
}

int RecieveComplete()
{
	return recieve_complete;
}
