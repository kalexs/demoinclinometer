#include "time.h"
#include "led.h"

volatile static unsigned char transmit_data[255] = "";
volatile static uint16_t tr_iter;
volatile static int tr_flag;
volatile static unsigned char recieved_data[255] = "";
volatile static uint16_t re_iter;
volatile static int re_flag;

volatile static int recieve_complete;

volatile static int transmit_buffer_size;

void USARTInit(void);


void push_char (uint8_t data);
void push_data (int16_t ang);
void push_data_32 (int ang);


uint8_t GetRecievedData(int n);

void clear_recieved_data (void);
void clear_transmit_data(void);

void ThrowReFlag(void);
void SetTrFlag(void);

void SetTrIter (uint16_t n);

uint16_t GetReIter(void);
uint16_t GetReFlag(void);
uint16_t GetTrFlag(void);

void TransmitStart(void);

unsigned char * GetRecieveDataPointer (void);
uint16_t GetRecieveDataLen(void);

unsigned char * GetTransmitDataPointer (void);
uint16_t GetTransmitDataLen(void);

int RecieveComplete(void);
